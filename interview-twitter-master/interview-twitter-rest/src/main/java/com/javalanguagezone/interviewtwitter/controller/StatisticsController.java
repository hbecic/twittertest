package com.javalanguagezone.interviewtwitter.controller;

import com.javalanguagezone.interviewtwitter.service.TweetService;
import com.javalanguagezone.interviewtwitter.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("statistics")
public class StatisticsController {

  private TweetService tweetService;
  private UserService userService;

  public StatisticsController(TweetService tweetService, UserService userService) {
    this.tweetService = tweetService;
    this.userService = userService;
  }

  @GetMapping(value = "{username}")
  public int[] getStatistics(@PathVariable String username) {
    int[] statistics = new int[3];
    statistics[0] = userService.getFollowingNumber(username);
    statistics[1] = userService.getFollowersNumber(username);
    statistics[2] = tweetService.numberOfTweets(username);
    return statistics;
  }
}
