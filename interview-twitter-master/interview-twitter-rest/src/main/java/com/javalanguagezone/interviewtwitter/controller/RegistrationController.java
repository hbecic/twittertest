package com.javalanguagezone.interviewtwitter.controller;

import com.javalanguagezone.interviewtwitter.domain.User;
import com.javalanguagezone.interviewtwitter.service.UserService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("register")
public class RegistrationController {

  private UserService userService;

  public RegistrationController(UserService userService) {
    this.userService = userService;
  }

  @CrossOrigin(origins = "*")
  @GetMapping(value = "checkUser/{username}")
  public Boolean usernameIsAvailable(@PathVariable String username){
   try {
     userService.loadUserByUsername(username);
   }catch (UsernameNotFoundException e){
     return true;
   }
   return false;
  }

  @CrossOrigin(origins = "*")
  @PostMapping(value = "new")
  public Boolean registerNewUser(@RequestBody User user){
    User newUser = userService.registerNewUser(user);
    return newUser != null;
  }
}
