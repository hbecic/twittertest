import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTweetsStatisticsComponent } from './user-tweets-statistics.component';

describe('UserTweetsStatisticsComponent', () => {
  let component: UserTweetsStatisticsComponent;
  let fixture: ComponentFixture<UserTweetsStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTweetsStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTweetsStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
