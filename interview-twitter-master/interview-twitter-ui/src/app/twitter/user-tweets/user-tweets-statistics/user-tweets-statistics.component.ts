import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-user-tweets-statistics',
  templateUrl: './user-tweets-statistics.component.html',
  styleUrls: ['./user-tweets-statistics.component.css']
})
export class UserTweetsStatisticsComponent {

  @Input() statistics: number[];

}
