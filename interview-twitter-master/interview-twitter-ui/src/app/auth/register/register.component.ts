import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  private userForm: FormGroup;
  private usernameIsValid: boolean;
  private registerSuccess = false;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private cd: ChangeDetectorRef, private router: Router) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      username: [''],
      password: [''],
      fullName: [''],
    });
  }

  checkUsername() {
     this.http.get('http://localhost:8080/api/register/checkUser/' + this.userForm.value.username,
       {responseType: 'text'}).subscribe(
       (res) => {
         res === 'true' ? this.usernameIsValid = true : this.usernameIsValid = false;
         this.cd.detectChanges();
       }

     );
  }

  onSubmit() {
    this.http.post('http://localhost:8080/api/register/new', JSON.stringify(this.userForm.value),
      {headers: {'Content-Type': 'application/json'}}).subscribe(
      () => {
        this.registerSuccess = true;
        this.userForm.reset();
        this.cd.detectChanges();
      }
    );
  }

  navigateToLogin() {
    this.router.navigateByUrl('/login');
  }
}
